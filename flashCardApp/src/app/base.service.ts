import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  private baseUpdated = new Subject<any>();

  base = {

  }

  constructor() { }

  getBase() {
    return this.base;
  }

  getBaseUpdateListener() {
    return this.baseUpdated.asObservable();
  }

  updateBase(incomingBase: any) {
    this.base = incomingBase;
    this.baseUpdated.next(incomingBase);
  }

  updateBasePromise(incomingBase: any) {
    new Promise( (response, reject) => {
      this.base = incomingBase;
      this.baseUpdated.next(incomingBase);
      response([]);
    })
  }




}
